# reuse-it 

Platform to borrow, exchange and transfer things between neighbours. Project for [Fixathon](https://fixathon.io) contest.

## Why this idea?
Climate changes are a fact. Humen activity is the most important reason of this situation. Reduction of consumption is one of possibles ways to help out planet. In Responsible Waste Management Hierarchy Reduce and Reuse steps has the highest priority and costs less effort from ordinary people. My platform started from this idea.

[![Waste Management Hierarchy: Reduce, Reuse, Recycle, Recovery, Disposal](https://waste4change.com/wp-content/uploads/Waste-Management-Hierarchy-1-1160x773.jpg)](https://waste4change.com/)

You don't need have your own iron when you want to iron your own shirt once a year. Maybe your neighbour can lend it for you? What if your children are bored with old books? Maybe somewhere in neighourhood is mother with similar childrem but other books? You don't need own things to use it. 

## Technical details:
- Node JS
- Hapi.js
- VueJS

## Installation 
1. Clone this repository using ssh or https: 
    - `git@gitlab.com:korneliakobiela/reuse-it.git`
    - `https://gitlab.com/korneliakobiela/reuse-it.git`
2. Open the console in project directory
3. Install dependencies `npm install`
4. Start the application `npm start`

