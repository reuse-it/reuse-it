const Product = require( '../model/Product' );
const validator = require( 'validator' );

function validateProduct(productData) {
    // TODO: implement it
}

async function addProduct(data) {
    const product = await new Product(data);

    await product.save();

    return product;
}

async function editProduct(id, data) {
    return Product.findOneAndReplace({_id: id}, data).exec();
}

async function getProducts() {
    return Product.find().exec();
}

async function getProduct(id) {
    return Product.findById(id).exec();
}

async function deleteProduct(id) {
    return Product.deleteOne({_id: id}).exec();
}

module.exports = {
    addProduct,
    editProduct,
    getProducts,
    getProduct,
    deleteProduct
};