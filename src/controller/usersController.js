const User = require('../model/User');
const validator = require('validator');
const bcrypt = require('bcrypt');

const saltRounds = 10;

function validateUser(userData) {
    // TODO: implement it
}

async function createUser(data) {
    data.password = await bcrypt.hash(data.password, saltRounds);
    const user = await new User(data);

    await user.save();

    return user;
}

async function showUserData(username) {
    return User.findOne({username}).exec();
}

async function editUserData(username, data) {
    return User.findOneAndReplace({username}, data).exec();
}

async function deleteUserAccount(username) {
    return User.deleteOne({username}).exec();
}

module.exports = {
    createUser,
    showUserData,
    editUserData,
    deleteUserAccount
};