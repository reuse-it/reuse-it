'use strict';
const server = require('./server/server');
const db = require('./server/db');

const init = async () => {
    await db();
    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init()