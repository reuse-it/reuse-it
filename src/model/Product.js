const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
    name: String,
    type: {
        type: String,
        enum: ['transfer', 'exchange', 'lend']
    },
    description: String,
    user: String,
    status: {
        type: String,
        enum: ['available', 'transfered', 'archived', 'hidden']
    }
});

module.exports = mongoose.model('Product', productSchema);
