const productsController = require('../controller/productsController');

module.exports = {
    addProduct: {
        method: 'POST',
        path: '/product',
        handler: async (request, h) => {
            const product = await productsController.addProduct(request.payload);

            return h.response({id: product._id}).location(`/product/${ product._id }`).code( 201 );
        }
    },
    getProducts: {
        method: 'GET',
        path: '/product',
        handler: async (request, h) => {
            const products = await productsController.getProducts();

            return h.response(products).code(200);
        }
    },
    editProduct: {
        method: 'PUT',
        path: '/product/{productid}',
        handler: async (request, h) => {
            const product = await productsController.editProduct(request.params.productid, request.payload);

            return h.response(product).code(200);
        }

    },
    getProductInfo: {
        method: 'GET',
        path: '/product/{productid}',
        handler: async (request, h) => {
            const product = await productsController.getProduct(request.params.productid);

            return h.response(product).code(200);
        }
    },
    deleteProduct: {
        method: 'DELETE',
        path: '/product/{productid}',
        handler: async (request, h) => {
            const product = await productsController.deleteProduct(request.params.productid);

            return h.response('').code(204);
        }

    },


}