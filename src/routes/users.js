const usersController = require('../controller/usersController');
module.exports = {
    createUser: {
        method: 'POST',
        path: '/user',
        handler: async (request, h) => {
            const user = await usersController.createUser(request.payload);

            return h.response({username: user.username}).location(`/user/${ user.username }`).code( 201 );
        }
    },
    showUserData: {
        method: 'GET',
        path: '/user/{username}',
        handler: async (request, h) => {
            const user = await usersController.showUserData(request.params.username);

            return h.response(user).code(200);
        }
    },
    editUserData: {
        method: 'PUT',
        path: '/user/{username}',
        handler: async (request, h) => {
            const user = await usersController.editUserData(request.params.username, request.payload);

            return h.response(user).code(200);
        }
    },
    deleteUserAccount: {
        method: 'DELETE',
        path: '/user/{username}',
        handler: async (request, h) => {
            const user = await usersController.deleteUserAccount(request.params.username);

            return h.response('').code(204);
        }
    }
}