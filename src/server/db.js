const mongoose = require('mongoose');
const db = mongoose.connection;


module.exports = async function() {
    mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log('Connected to database');
    });
}