const Hapi = require('@hapi/hapi');
const users = require('../routes/users');
const products = require('../routes/products');

const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    /**
     * Routes responsible for user account
     */
    server.route(users.createUser);
    server.route(users.showUserData);
    server.route(users.editUserData);
    server.route(users.deleteUserAccount);

    /**
     * Routes responsible for products
     */
    server.route(products.addProduct);
    server.route(products.getProducts);
    server.route(products.editProduct);
    server.route(products.getProductInfo);
    server.route(products.deleteProduct);

module.exports = server;